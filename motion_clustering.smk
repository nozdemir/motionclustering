"""

Snakefile for the motion clustering project.
Execution: sbatch snakemake
Plot DAG: snakemake --snakefile motion_clustering.smk --forceall --dag | dot -Tpdf > motion_clustering.pdf
Plot rule graph: snakemake --snakefile motion_clustering.smk --forceall --rulegraph | dot -Tpdf > motion_clustering.pdf

"""

outpath = "train_results/"

model_type = ["AE", "VAE", "VQVAE"]
encoder_model = ["recurrent", "TCN", "transformer"]
latent_dimension = [2, 4, 8, 16]


rule run_experiments:
    input:
        # Train a variety of models
        expand(
            outpath
            + "{model_type}_{encoder_model}_{latent_dimension}_final_weights.index",
            model_type=model_type,
            encoder_model=encoder_model,
            latent_dimension=latent_dimension,
        ),


rule train_models:
    input:
        input_data="fishingData.csv",
    output:
        model_weights=outpath
        + "{model_type}_{encoder_model}_{latent_dimension}_final_weights.index",
    shell:
        "python run_experiment.py "
        "-i {input.input_data} "
        "-m {wildcards.model_type} "
        "-e {wildcards.encoder_model} "
        "-z {wildcards.latent_dimension} "
        "-o " + outpath
