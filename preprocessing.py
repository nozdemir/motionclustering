import copy
import numpy as np
import ruptures as rpt
from sklearn.feature_selection import VarianceThreshold
import pandas as pd


def split_with_breakpoints(a: np.ndarray, breakpoints: list) -> np.ndarray:
    """

    Split a numpy.ndarray at the given breakpoints.

    Args:
        a (np.ndarray): N (instances) * m (features) shape
        breakpoints (list): list of breakpoints obtained with ruptures

    Returns:
        split_a (np.ndarray): padded array of shape N (instances) * l (maximum break length) * m (features)

    """
    rpt_lengths = list(np.array(breakpoints)[1:] - np.array(breakpoints)[:-1])

    try:
        max_rpt_length = np.max([breakpoints[0], np.max(rpt_lengths)])
    except ValueError:
        max_rpt_length = breakpoints[0]

    # Reshape experiment data according to extracted ruptures
    split_a = np.split(np.expand_dims(a, axis=0), breakpoints[:-1], axis=1)

    split_a = [
        np.pad(
            i, ((0, 0), (0, max_rpt_length - i.shape[1]), (0, 0)), constant_values=0.0
        )
        for i in split_a
    ]
    split_a = np.concatenate(split_a, axis=0)

    return split_a


def rolling_window(
    a: np.array,
    window_size: int,
    window_step: int,
    automatic_changepoints: str = False,
    precomputed_breaks: np.ndarray = None,
) -> np.ndarray:
    """

    Returns a 3D numpy.array with a sliding-window extra dimension.

    Args:
        a (np.ndarray): N (instances) * m (features) shape
        window_size (int): Size of the window to apply
        window_step (int): Step of the window to apply
        automatic_changepoints (str): Changepoint detection algorithm to apply.
        If False, applies a fixed sliding window.
        precomputed_breaks (np.ndarray): Precomputed breaks to use, bypassing the changepoint detection algorithm.
        None by default (break points are computed).

    Returns:
        rolled_a (np.ndarray): N (sliding window instances) * l (sliding window size) * m (features)

    """

    breakpoints = None

    if automatic_changepoints:
        # Define change point detection model using ruptures
        # Remove dimensions with low variance (occurring when aligning the animals with the y axis)
        if precomputed_breaks is None:
            rpt_model = rpt.KernelCPD(
                kernel=automatic_changepoints, min_size=window_size, jump=window_step
            ).fit(VarianceThreshold(threshold=1e-3).fit_transform(a))

            # Extract change points from current experiment
            breakpoints = rpt_model.predict(pen=4.0)

        else:
            breakpoints = np.cumsum(precomputed_breaks)

        rolled_a = split_with_breakpoints(a, breakpoints)

    else:
        shape = (a.shape[0] - window_size + 1, window_size) + a.shape[1:]
        strides = (a.strides[0],) + a.strides
        rolled_a = np.lib.stride_tricks.as_strided(
            a, shape=shape, strides=strides, writeable=True
        )[::window_step]

    return rolled_a, breakpoints


def rotate(
    p: np.array, angles: np.array, origin: np.array = np.array([0, 0])
) -> np.array:
    """

    Returns a 2D numpy.ndarray with the initial values rotated by angles radians.

    Args:
        p (numpy.ndarray): 2D Array containing positions of bodyparts over time.
        angles (numpy.ndarray): Set of angles (in radians) to rotate p with.
        origin (numpy.ndarray): Rotation axis (zero vector by default).

    Returns:
        - rotated (numpy.ndarray): rotated positions over time

    """

    R = np.array([[np.cos(angles), -np.sin(angles)], [np.sin(angles), np.cos(angles)]])

    o = np.atleast_2d(origin)
    p = np.atleast_2d(p)

    rotated = np.squeeze((R @ (p.T - o.T) + o.T).T)

    return rotated


# noinspection PyArgumentList
def align_trajectories(data: np.array, mode: str = "all") -> np.array:
    """

    Returns a numpy.array with the positions rotated in a way that the center (0 vector)
    and the body part in the first column of data are aligned with the z axis.

    Args:
        data (numpy.ndarray): 3D array containing positions of body parts over time, where
        shape is N (sliding window instances) * m (sliding window size) * l (features)
        mode (string): Specifies if *all* instances of each sliding window get
        aligned, or only the *center*

    Returns:
        aligned_trajs (np.ndarray): 2D aligned positions over time.

    """

    angles = np.zeros(data.shape[0])
    data = copy.deepcopy(data)
    dshape = data.shape

    if mode == "center":
        center_time = (data.shape[1] - 1) // 2
        angles = np.arctan2(data[:, center_time, 0], data[:, center_time, 1])
    elif mode == "all":
        data = data.reshape(-1, dshape[-1], order="C")
        angles = np.arctan2(data[:, 0], data[:, 1])
    elif mode == "none":
        data = data.reshape(-1, dshape[-1], order="C")
        angles = np.zeros(data.shape[0])

    aligned_trajs = np.zeros(data.shape)

    for frame in range(data.shape[0]):
        aligned_trajs[frame] = rotate(
            data[frame].reshape([-1, 2], order="C"), angles[frame]
        ).reshape(data.shape[1:], order="C")

    if mode == "all" or mode == "none":
        aligned_trajs = aligned_trajs.reshape(dshape, order="C")

    return aligned_trajs


def center_skeleton(to_center: pd.DataFrame, body_part: str = "Head") -> pd.DataFrame:
    """

    Returns a DataFrame with the full skeleton centered around the specified body part,
    along the XZ plane.

    Args:
        to_center (pd.DataFrame): table containing raw coordinates.
        body_part (string): Identifier for the body part to center on.

    Returns:
        centered (pd.DataFrame): table containing centered coordinates.

    """

    centered = copy.deepcopy(to_center)

    head = centered.loc[:, [col for col in centered.columns if body_part in col]]

    # Center X
    centered.update(centered.iloc[:, 0::3] - np.expand_dims(head.values[:, 0], 1))

    # Center Z
    centered.update(centered.iloc[:, 2::3] - np.expand_dims(head.values[:, 2], 1))

    return centered


def rotate_skeleton(to_rotate: pd.DataFrame, body_part: str = "Eyes") -> pd.DataFrame:
    """

    Returns a DataFrame with the full skeleton rotated around the specified body part,
    along the XZ plane.

    Args:
        to_rotate (pd.DataFrame): table containing raw coordinates.
        body_part (string): Identifier for the body part to rotate on.

    Returns:
        aligned_train (pd.DataFrame): table containing rotated coordinates.

    """

    # In order to apply the rotation alignment function, we need to bring the desired body part to the front (needs to use the
    # first few columns of the DataFrame)
    sorted_list = [col for col in to_rotate.columns if body_part in col] + sorted(
        [col for col in to_rotate.columns if body_part not in col]
    )
    to_rotate = to_rotate.loc[:, sorted_list]

    # Get the names of all the columns to use (leaving Y aside), and get a filtered dataframe with only those columns
    x_z_cols = [col for col in to_rotate.columns if not "_y" in col]
    x_z_coords = to_rotate.loc[:, x_z_cols].values

    # Apply the rotation function to the newly created dataframe
    aligned_x_z = align_trajectories(x_z_coords, mode="all")

    # Update a copy of the centered data with the rotated values
    aligned_train = copy.deepcopy(to_rotate)
    aligned_train.update(pd.DataFrame(aligned_x_z, columns=x_z_cols))

    return aligned_train


def sliding_window_per_subject(subjects, train, window_length, window_stride):
    """

    subjects (np.ndarray): subjects IDs per timepoint
    train (np.ndarray): 2D matrix with body part position per timepoint

    """

    sliding_windows = []

    # Iterate over all the IDs
    for s in subjects.unique():
        df = train.loc[subjects == s, :].values

        # Get the rolling window
        r_w = rolling_window(df, window_length, window_stride)[0]

        # Add the rolling window to a collection
        sliding_windows.append(r_w)

    # Concatenate the collection into a single array and return
    return np.concatenate(sliding_windows)
