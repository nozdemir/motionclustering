#!/bin/sh

mkdir snakemake_logs

module load cuda/11.2
module load cudnn

snakemake --snakefile motion_clustering.smk --forceall --dag | dot -Tpdf > motion_clustering.pdf
snakemake --snakefile motion_clustering.smk --forceall --rulegraph | dot -Tpdf > motion_clustering.pdf

snakemake --snakefile motion_clustering.smk run_experiments -j 100 --latency-wait 15 --cluster-config cluster.json --cluster "sbatch --time={cluster.time} --mem={cluster.mem} -o {cluster.stdout} -e {cluster.stderr} --job-name={cluster.jobname} --cpus-per-task={cluster.cpus} --constraint="gpu" --gres=gpu:gtx980:2" > motion_clustering.out 2> motion_clustering.err
