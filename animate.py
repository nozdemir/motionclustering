from IPython import display
from matplotlib.animation import FuncAnimation, FFMpegWriter
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


# noinspection PyTypeChecker
def animate_skeleton(
    data: pd.DataFrame,
    cluster_assignments=None,
    embedding=None,
    selected_cluster=None,
    save=None,
):
    """

    FuncAnimation function to plot motion trajectories over time

    Args:
        data (pd.DataFrame): table containing motion coordinates.
        cluster_assignments (np.ndarray): contain sorted cluster assignments for all instances in data.
        If provided together with selected_cluster, only instances of the specified component are returned.
        Defaults to None.
        only instances of the specified component are returned. Defaults to None.
        embedding (np.ndarray): UMAP 2D embedding of the datapoints provided. If not None, a second animation
        shows a parallel animation showing the currently selected embedding, colored by cluster if cluster_assignments
        are available.
        selected_cluster (int): cluster to filter. If provided together with cluster_assignments,
        save (str): name of the file where to save the procuced animation/

    Returns:
        aligned_train (pd.DataFrame): table containing rotated coordinates.

    """

    # Checks that all shapes and passed parameters are correct
    if embedding is not None:

        assert (
            embedding.shape[0] == data.shape[0]
        ), "there should be one embedding per row in data"

        if selected_cluster is not None:
            cluster_embedding = embedding[cluster_assignments == selected_cluster]

        else:
            cluster_embedding = embedding

    if cluster_assignments is not None:

        assert (
            len(cluster_assignments) == data.shape[0]
        ), "there should be one cluster assignment per row in data"

        # Filter data to keep only those instances assigned to a given cluster
        if selected_cluster is not None:

            assert selected_cluster in set(
                cluster_assignments
            ), "selected cluster should be in the clusters provided"

            data = data.loc[cluster_assignments == selected_cluster, :]

    # Define canvas
    fig = plt.figure(figsize=((10 if embedding is not None else 5), 5))

    # If embeddings are provided, add projection plot to the left
    if embedding is not None:
        ax1 = fig.add_subplot(121)

        # Plot entire UMAP
        ax1.scatter(
            embedding[:, 0],
            embedding[:, 1],
            c=(cluster_assignments if cluster_assignments is not None else None),
            cmap="tab10",
        )

        # Plot current position
        umap_scatter = ax1.scatter(
            cluster_embedding[0, 0],
            cluster_embedding[0, 1],
            color="red",
            s=75,
            linewidths=2,
            edgecolors="black",
        )

        ax1.set_title("UMAP projection of time embedding")
        ax1.set_xlabel("UMAP-1")
        ax1.set_ylabel("UMAP-2")

    # Add skeleton animation
    ax2 = fig.add_subplot((122 if embedding is not None else 111), projection="3d")

    # Plot!
    init_x = data.loc[:, [col for col in data.columns if "_x" in col]].iloc[0, :]
    init_y = data.loc[:, [col for col in data.columns if "_y" in col]].iloc[0, :]
    init_z = data.loc[:, [col for col in data.columns if "_z" in col]].iloc[0, :]

    skeleton_scatter = ax2.scatter(
        xs=np.array(init_x),
        ys=np.array(init_z),
        zs=np.array(init_y),
        color="#006699",
        label="Original",
    )

    # Update data in main plot
    def animation_frame(i):

        if embedding is not None:
            # Update umap scatter
            umap_x = cluster_embedding[i, 0]
            umap_y = cluster_embedding[i, 1]

            umap_scatter.set_offsets(np.c_[umap_x, umap_y])

        # Update skeleton scatter plot
        x = data.loc[:, [col for col in data.columns if "_x" in col]].iloc[i, :]
        y = data.loc[:, [col for col in data.columns if "_y" in col]].iloc[i, :]
        z = data.loc[:, [col for col in data.columns if "_z" in col]].iloc[i, :]

        skeleton_scatter._offsets3d = (np.array(x), np.array(z), np.array(y))

        if embedding is not None:
            return umap_scatter, skeleton_scatter

        return skeleton_scatter

    animation = FuncAnimation(
        fig,
        func=animation_frame,
        frames=data.shape[0],
        interval=50,
    )

    ax2.set_title("Motion animation")
    ax2.set_xlabel("x")
    ax2.set_ylabel("z")
    plt.legend()

    if save is not None:
        writergif = FFMpegWriter(fps=15)
        animation.save(save, writer=writergif)

    video = animation.to_html5_video()
    html = display.HTML(video)
    display.display(html)
    plt.close()
