# Import external packages
import argparse
import datetime
import os
import umap

from sklearn.preprocessing import StandardScaler
from tensorflow.keras.callbacks import EarlyStopping, TensorBoard

# Import internal packages
from animate import *
from preprocessing import *
from models import *


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


def main(args):

    ## Load and preprocess the data

    # Load raw data and remove irrelevant columns
    print("Loading and preprocessing data...")

    raw_data = pd.read_csv(args.input_data)
    subjects = raw_data.Subject
    raw_data = raw_data.drop(["realtime", "Subject"], axis=1)

    # Keep only columns with positions (remove rotations)
    raw_data = raw_data.loc[
        :, [col for col in raw_data.columns if "pos" in col and "Reference" not in col]
    ]
    centered_train = center_skeleton(raw_data, body_part="Hips")
    aligned_train = rotate_skeleton(centered_train, body_part="Eyes")

    normalised_train = aligned_train.copy()

    for subject in subjects.unique():

        standard_scaler = StandardScaler()
        train_X = aligned_train.loc[subjects == subject, :]

        train_X = pd.DataFrame(
            standard_scaler.fit_transform(train_X),
            index=train_X.index,
            columns=train_X.columns,
        )
        normalised_train.update(train_X)

    # Split the data into train and validation sets
    def train_val_split(subject_ids, proportion=0.8):
        """creates a train val split, assigning the given proportion of the data to the validation set"""

        train_number = int(len(subject_ids) * proportion)
        train_ids = list(np.random.choice(subject_ids, train_number, replace=False))

        val_ids = [x for x in subject_ids if x not in train_ids]

        return train_ids, val_ids

    train_ids, val_ids = train_val_split(
        subjects.unique(), proportion=args.test_proportion
    )
    train_split = normalised_train.loc[subjects.isin(train_ids), :]
    validation_split = normalised_train.loc[subjects.isin(val_ids), :]

    # Slice data into sliding windows, that will serve as inputs to the models
    train_sliding_windows = sliding_window_per_subject(
        subjects[subjects.isin(train_ids)],
        train_split,
        args.window_length,
        args.window_stride,
    )
    validation_sliding_windows = sliding_window_per_subject(
        subjects[subjects.isin(val_ids)],
        validation_split,
        args.window_length,
        args.window_stride,
    )

    print("Data loaded and preprocessed!")
    print("Training data shape:", train_sliding_windows.shape)
    print("Validation data shape:", validation_sliding_windows.shape)

    ## Train a model and log results

    print("Training specified model...")

    if args.model_type == "AE":
        full_model, encoder, decoder = LSTM_AE_baseline(
            input_dim=train_sliding_windows.shape[-1],
            timesteps=args.window_length,
            latent_dim=args.latent_dimension,
        )

        if not args.load_weights:
            full_model.compile(
                optimizer=tf.optimizers.Adam(),
                loss=tf.keras.losses.MeanSquaredError(),
                metrics=["MSE"],
            )

            full_model.fit(
                train_sliding_windows,
                train_sliding_windows,
                epochs=500,
                validation_data=(
                    validation_sliding_windows,
                    validation_sliding_windows,
                ),
                callbacks=[
                    EarlyStopping(
                        monitor="val_loss", patience=10, restore_best_weights=True
                    ),
                    TensorBoard(
                        log_dir=os.path.join(
                            args.output_path,
                            f"AE_{args.encoder_type}_{args.latent_dimension}",
                        )
                    ),
                ],
            )

    elif args.model_type == "VAE":
        full_model, encoder, decoder = LSTM_VAE_baseline(
            original_dim=train_sliding_windows.shape[-1],
            time_points=args.window_length,
            intermediate_dim=args.latent_dimension * 4,
            latent_dim=args.latent_dimension,
        )

        if not args.load_weights:
            full_model.compile(
                optimizer=tf.optimizers.Adam(),
                loss=tf.keras.losses.MeanSquaredError(),
                metrics=["MSE"],
            )

            full_model.fit(
                train_sliding_windows,
                train_sliding_windows,
                epochs=500,
                validation_data=(
                    validation_sliding_windows,
                    validation_sliding_windows,
                ),
                callbacks=[
                    EarlyStopping(
                        monitor="val_loss", patience=10, restore_best_weights=True
                    ),
                    TensorBoard(
                        log_dir=os.path.join(
                            args.output_path,
                            f"VAE_{args.encoder_type}_{args.latent_dimension}",
                        )
                    ),
                ],
            )

    elif args.model_type == "VQVAE":

        full_model = VQVAE(
            input_shape=train_sliding_windows.shape,
            latent_dim=args.latent_dimension,
            n_components=args.number_of_components,
            encoder_type=args.encoder_type,
        )

        full_model.optimizer = tf.keras.optimizers.Nadam(
            learning_rate=1e-4, clipvalue=0.75
        )

        if not args.load_weights:
            full_model.compile(optimizer=full_model.optimizer, run_eagerly=False)

            full_model.fit(
                train_sliding_windows,
                train_sliding_windows,
                epochs=500,
                validation_data=(
                    validation_sliding_windows,
                    validation_sliding_windows,
                ),
                callbacks=[
                    EarlyStopping(
                        monitor="val_total_loss", patience=10, restore_best_weights=True
                    ),
                    TensorBoard(
                        log_dir=os.path.join(
                            args.output_path,
                            f"VQVAE_{args.encoder_type}_{args.latent_dimension}_{args.number_of_components}",
                        )
                    ),
                ],
            )

    else:
        raise ValueError("Invalid model type specified")

    # Save weights to disk
    today = datetime.datetime.today().strftime("%Y%m%d")

    if not args.load_weights:
        full_model.save_weights(
            os.path.join(
                args.output_path,
                "{}_{}_{}_final_weights".format(
                    args.model_type, args.encoder_type, args.latent_dimension
                ),
            )
        )
    else:
        full_model.load_weights(
            os.path.join(
                args.output_path,
                "{}_{}_{}_final_weights".format(
                    args.model_type, args.encoder_type, args.latent_dimension
                ),
            )
        )

    # Generate VQVAE animations
    if args.output_animations and args.model_type == "VQVAE":

        print("Generating VQVAE embeddings...")
        autoencoder_reduced_dim = full_model.encoder(train_sliding_windows)

        cluster_assignments = full_model.soft_quantizer(train_sliding_windows)
        cluster_assignments = np.argmax(cluster_assignments.numpy(), axis=1)

        train_subjects = subjects[subjects.isin(train_ids)]
        subject_data = aligned_train[subjects.isin(train_ids)][
            train_subjects == "RR005"
        ]

        train_subjects_sw = np.repeat(
            train_subjects.unique(),
            (subjects == subjects[0]).sum() - train_sliding_windows.shape[1] + 1,
        )

        subject_umap = umap.UMAP(n_components=2).fit_transform(autoencoder_reduced_dim)[
            train_subjects_sw == "RR005"
        ]
        subject_assignments = cluster_assignments[train_subjects_sw == "RR005"]

        print(subject_data[12:-12].shape)
        print(subject_umap.shape)

        for cluster in list(set(cluster_assignments)) + ["all"]:

            try:
                animate_skeleton(
                    subject_data[12:-12],
                    subject_assignments,
                    subject_umap,
                    (cluster if cluster != "all" else None),
                    save=f"{args.model_type}_{args.encoder_type}_z={args.latent_dimension}_cluster={cluster}.mp4",
                )
            except AssertionError:
                continue


if __name__ == "__main__":
    ## CLI using argparse
    parser = argparse.ArgumentParser(
        description="Model training for the Motion Clustering project"
    )

    parser.add_argument(
        "--input-data",
        "-i",
        help="CSV file containing all data to use",
        type=str,
        default=None,
    )
    parser.add_argument(
        "--model-type",
        "-m",
        help="Model to train. Currently must be one of AE, VAE, or VQVAE",
        type=str,
        default="VAE",
    )
    parser.add_argument(
        "--encoder-type",
        "-e",
        help="Encoder architecture to use. Currently must be one of recurrent, TCN, or transformer",
        type=str,
        default="recurrent",
    )
    parser.add_argument(
        "--latent-dimension",
        "-z",
        help="Latent dimension for the embedding models",
        type=int,
        default=4,
    )
    parser.add_argument(
        "--output-path",
        "-o",
        help="Path where to save all the produced data",
        type=str,
        default="./results",
    )
    parser.add_argument(
        "--window-length",
        "-wl",
        help="Window lenght in sliding window",
        type=int,
        default=25,
    )
    parser.add_argument(
        "--window-stride",
        "-ws",
        help="Window stride in sliding window",
        type=int,
        default=1,
    )
    parser.add_argument(
        "--test-proportion",
        "-tp",
        help="Proportion of train data in all the data",
        type=float,
        default=0.8,
    )
    parser.add_argument(
        "--number-of-components",
        "-k",
        help="Number of clusters to expect in deep clustering models",
        type=int,
        default=5,
    )
    parser.add_argument(
        "--load-weights",
        "-w",
        help="Load pretrained weights instead of training the selected model from scratch",
        type=str2bool,
        default=False,
    )
    parser.add_argument(
        "--output-animations",
        "-anim",
        help="If True, create and output embedding animations for all subjects in the dataset and for each individual cluster",
        type=str2bool,
        default=False,
    )

    args = parser.parse_args()
    main(args)
